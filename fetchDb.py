from SPARQLWrapper import SPARQLWrapper, JSON


def get_results(entity):
    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    sparql.setQuery("""
    PREFIX dbpedia: <http://dbpedia.org/resource/>
    PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>

    SELECT DISTINCT ?entity ?abstract ?type WHERE {
    ?entity dbpedia-owl:abstract ?abstract ;
            foaf:name ?name ;
            rdf:type ?type ;
            rdfs:label ?label .
    FILTER CONTAINS(?name,\"""" + entity + """\")
    FILTER CONTAINS(?label, \"""" + entity + """\")
    FILTER CONTAINS(?abstract, \"""" + entity + """\")
    FILTER CONTAINS(str(?type), str(<http://dbpedia.org/ontology/>))
    FILTER(langMatches(lang(?abstract),"en"))
    }
    ORDER BY strlen(?entity)
    """)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    flattenedResults = []
    it = 0
    ite = 0
    for result in results["results"]["bindings"]:
        if(it == 0):
            flattenedResults.append(str(results["results"]["bindings"][ite]["entity"]["value"]) + " | " + str(results["results"]["bindings"][ite]
                                                                                                              ["abstract"]["value"]) + " | " + str(results["results"]["bindings"][ite]["type"]["value"]).lstrip('http://dbpedia.org/ontology/'))
        else:
            if(str(results["results"]["bindings"][it-1]["entity"]["value"]) == str(result["entity"]["value"])):
                if(str(result["type"]["value"]) not in flattenedResults[ite]):
                    flattenedResults[ite] = flattenedResults[ite] + " | " + str(
                        result["type"]["value"]).lstrip('http://dbpedia.org/ontology/')
            else:
                flattenedResults.append(str(result["entity"]["value"]) + " | " + str(
                    result["abstract"]["value"]) + " | " + str(result["type"]["value"]).lstrip('http://dbpedia.org/ontology/'))
                ite = ite+1
        it = it+1

    return flattenedResults


typeDict = {
        "PERSON": "Agent",
        "NORP": "EthnicGroup",
        "FAC": "Place",
        "ORG": "Agent",
        "GPE": "Place",
        "LOC": "Place",
        "PRODUCT": True,
        "EVENT": True,
        "WORK_OF_ART": "Work",
        "LAW": "Agent",
        "LANGUAGE": "Language",
        "DATE": False,
        "PERCENT": False,
        "MONEY": False,
        "QUANTITY": False,
        "ORDINAL": False,
        "CARDINAL": False
    }