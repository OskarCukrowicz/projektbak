Write-Host "Installing environment" -ForegroundColor green
$pythonVersion = python --version

If(!$pythonVersion -match "[A-Za-z]+ 3\.7."){
    Write-Host "Script was tested on python version 3.7.x, it might not work on your version" -ForegroundColor red
}

pip install spacy
python -m spacy download en_core_web_sm
pip install SPARQLWrapper
pip install gensim
pip install numpy
pip install scipy