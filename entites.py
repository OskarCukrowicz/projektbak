import spacy
from spacy import displacy
from collections import Counter
nlp =  spacy.load('en_core_web_sm')

def getEntities(text):
    doc = nlp(text)
    return doc.ents