from gensim import downloader
import numpy as np
import scipy
model = downloader.load("glove-twitter-25")  # glove-wiki-gigaword-100


def gen_mean(vals, p):
    p = float(p)
    return np.power(
        np.mean(
            np.power(
                np.array(vals, dtype=complex),
                p),
            axis=0),
        1 / p
    )


operations = dict([
    ('mean', (lambda word_embeddings: [
     np.mean(word_embeddings, axis=0)], lambda embeddings_size: embeddings_size)),
    ('max', (lambda word_embeddings: [
     np.max(word_embeddings, axis=0)], lambda embeddings_size: embeddings_size)),
    ('min', (lambda word_embeddings: [
     np.min(word_embeddings, axis=0)], lambda embeddings_size: embeddings_size)),
    ('p_mean_0.5', (lambda word_embeddings: [gen_mean(
        word_embeddings, p=0.5).real], lambda embeddings_size: embeddings_size)),
    ('p_mean_2', (lambda word_embeddings: [gen_mean(
        word_embeddings, p=2.0).real], lambda embeddings_size: embeddings_size)),
    ('p_mean_3', (lambda word_embeddings: [gen_mean(
        word_embeddings, p=3.0).real], lambda embeddings_size: embeddings_size)),
    ('p_mean_4', (lambda word_embeddings: [gen_mean(
        word_embeddings, p=4.0).real], lambda embeddings_size: embeddings_size)),
    ('p_mean_5', (lambda word_embeddings: [gen_mean(
        word_embeddings, p=5.0).real], lambda embeddings_size: embeddings_size)),
])


def get_sentence_embedding(word_embeddings, chosen_operations):
    concat_embs = []
    for o in chosen_operations:
        concat_embs += operations[o][0](word_embeddings)
    sentence_embedding = np.concatenate(
        concat_embs,
        axis=0
    )

    return sentence_embedding


def batcher(documents):
    ret = []

    for document in documents:
        embeddings = []
        for word in document:
            if word in model.wv:
                embeddings.append(model.wv[word])
        ret.append(get_sentence_embedding(
            embeddings, ['p_mean_2', 'p_mean_3']))

    ret = np.vstack(ret)
    return ret


def getBestCandidatePosition(baseVector, listOfCandidatesVectors):
    simmilarities = []
    for vector in listOfCandidatesVectors:
        similarity = scipy.spatial.distance.cosine(baseVector, vector)
        simmilarities.append(similarity)

    return simmilarities.index(max(simmilarities))
