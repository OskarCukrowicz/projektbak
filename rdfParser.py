import requests
from lxml import html
import json


def rdfToObject(rdfContent):
    url = 'http://www.easyrdf.org/converter'
    data = {
        'data': rdfContent,
        'uri': 'test',
        'in': 'turtle',
        'out': 'jsonld'
    }
    pageContent = requests.post(url, data).text
    tree = html.fromstring(pageContent)
    code = tree.xpath('//code')
    return json.loads(code[0].text)[0]


def outPutParser(text, entitesOutput):
    output = """@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix nif: <http://persistence.uni-leipzig.org/nlp2rdf/ontologies/nif-core#> .
@prefix dbpedia: <http://dbpedia.org/resource/> .
@prefix itsrdf: <http://www.w3.org/2005/11/its/rdf#> ."""

    output += f"""<http://example.com/example-task1#char=0,{len(text)}>
        a                     nif:RFC5147String , nif:String , nif:Context ;
        nif:beginIndex        "0"^^xsd:nonNegativeInteger ;
        nif:endIndex          "{len(text)}"^^xsd:nonNegativeInteger ;
        nif:isString          "{text}"@en ."""
    output += '\n'
    for item in entitesOutput:
        if(item['found']):
            output += f"""<http://example.com/example-task1#char={item['startIndex']},{item['endIndex']}>
            a                     nif:RFC5147String , nif:String ;
            nif:anchorOf          "{item['entity']}"@en ;
            nif:beginIndex        "{item['startIndex']}"^^xsd:nonNegativeInteger ;
            nif:endIndex          "{item['endIndex']}"^^xsd:nonNegativeInteger ;
            nif:referenceContext  <http://example.com/example-task1#char=0,{len(text)}> ;
            itsrdf:taIdentRef     dbpedia:{item['dbpedia'].lstrip('http://dbpedia.org/resource/')} ."""
        else:
            output+=f"""<http://example.com/example-task1#char={item['startIndex']},{item['endIndex']}>
            a                     nif:RFC5147String , nif:String ;
            nif:anchorOf          "{item['entity']}"@en ;
            nif:beginIndex        "{item['startIndex']}"^^xsd:nonNegativeInteger ;
            nif:endIndex          "{item['endIndex']}"^^xsd:nonNegativeInteger ;
            nif:referenceContext  <http://example.com/example-task1#char=0,{len(text)}> ;
            itsrdf:taIdentRef     <http://aksw.org/notInWiki/{item['entity'].replace(' ','_')}> ."""
        output+='\n'

    return output