#import sentenceEmbedding
import fetchDb
import entites
import rdfParser
import sentenceEmbedding
def findIndexes(word, sentence):
    return sentence.find(word), sentence.find(word) + len(word)

input = """@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix nif: <http://persistence.uni-leipzig.org/nlp2rdf/ontologies/nif-core#> .
@prefix dbpedia: <http://dbpedia.org/resource/> .
@prefix itsrdf: <http://www.w3.org/2005/11/its/rdf#> .
<http://example.com/example-task1#char=0,146>
        a                     nif:RFC5147String , nif:String , nif:Context ;
        nif:beginIndex        "0"^^xsd:nonNegativeInteger ;
        nif:endIndex          "146"^^xsd:nonNegativeInteger ;
        nif:isString          "Florence May Harding studied at a school in Sydney, and with Douglas Robert Dundas , but in effect had no formal training in either botany or art."@en ."""


parsedInput = rdfParser.rdfToObject(input)
text = parsedInput['http://persistence.uni-leipzig.org/nlp2rdf/ontologies/nif-core#isString'][0]['@value']
namedEntities = entites.getEntities(text)

result = []
for X in namedEntities:
    namedEntityClass = fetchDb.typeDict[X.label_]
    word2VecCandidates = []
    candidateFound = False
    if(type(namedEntityClass) is bool):
        if(namedEntityClass):
            word2VecCandidates = fetchDb.get_results(X.text)
        else:
            continue
    else:
        filterableCandidates = fetchDb.get_results(X.text)
        for candidate in filterableCandidates:
            parsedCandidate = candidate.split('|')
            name = parsedCandidate[0].strip().lstrip(
                'http://dbpedia.org/resource/')
            if(name == X.text.replace(' ', '_')):
                word2VecCandidates.append(candidate)
                candidateFound = True
                break
            candidateClasses = parsedCandidate[2:]
            for c in candidateClasses:
                if(str(c).strip() == str(namedEntityClass).strip()):
                    word2VecCandidates.append(candidate)

    if(len(word2VecCandidates) == 0):
        start, end = findIndexes(X.text, text)
        result.append({
            'entity': X.text,
            'found': False,
            'startIndex': start,
            'endIndex': end,
        })
        continue

    if(len(word2VecCandidates) == 1):
        start, end = findIndexes(X.text, text)
        result.append({
            'startIndex': start,
            'endIndex': end,
            'entity': X.text,
            'dbpedia': word2VecCandidates[0].split('|')[0],
            'found': True
        })
        continue
    else:
        mainVector = sentenceEmbedding.batcher([text])[0]
        candidatesVectors = []
        for candidate in word2VecCandidates:
            parsedCandidate = candidate.split('|')
            candidateText = parsedCandidate[1]
            candidatesVectors.append(
                sentenceEmbedding.batcher([candidateText])[0])
        bestCandidate = sentenceEmbedding.getBestCandidatePosition(
            mainVector, candidatesVectors)
        start, end = findIndexes(X.text, text)
        result.append({
            'startIndex': start,
            'endIndex': end,
            'entity': X.text,
            'dbpedia': word2VecCandidates[bestCandidate].split('|')[0],
            'found': True
        })

print(rdfParser.outPutParser(text,result))